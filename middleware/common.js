import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import express from 'express';

export default function CommonMiddleware(app) {
    app.use(express.urlencoded({ extended: true }));
    app.use(express.json({ extended: true }));
    app.use(morgan('common'));
    app.use(cors());
    app.use(helmet());
}
