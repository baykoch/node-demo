/* eslint-disable no-console */
import chalk from 'chalk';

import {
    AuthenticationError,
    ValidationError,
    AccessDeniedError,
} from '../errors/errors';

/**
 * Log every error event.
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
function errrorLogger(err, req, res, next) {
    if (err?.message) {
        console.log(chalk.red(err.message));
    }
    if (err?.stack) {
        console.log(chalk.red(err.stack));
    }
    next(err);
}

/**
 * Authentication error handler
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
function authenticationErrorHandler(err, req, res, next) {
    if (err instanceof AuthenticationError) {
        res.sendStatus(401);
    } else next(err);
}

/**
 * Validation error handler
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
function validationErrorHandler(err, req, res, next) {
    if (err instanceof ValidationError) {
        res.sendStatus(400);
    } else next(err);
}

/**
 * Access denied error handler
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
function accessDeniedErrorHandler(err, req, res, next) {
    if (err instanceof AccessDeniedError) {
        res.sendStatus(403);
    } else next(err);
}

/**
 * Built-in errors(standart)
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
function genericErrorHandler(err, req, res, next) {
    res.sendStatus(500);
}

export default function ErrorHandlerMiddleware(app) {
    app.use([
        errrorLogger,
        authenticationErrorHandler,
        validationErrorHandler,
        accessDeniedErrorHandler,
        genericErrorHandler,
    ]);
}
