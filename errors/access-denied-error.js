class AccessDeniedError extends Error {
    
    #model = ''

    #name = ''

    constructor(message, model) {
        super(message);
        this.#name = 'AccessDeniedError';
        this.#model = model;
    }

    get getName() {
        return this.#name;
    }

    set setName(name) {
        this.#name = name;
    }

    set setModel(model) {
        this.#model = model;
    }

    get getModel() {
        return this.#model;
    }
}

export default AccessDeniedError;
