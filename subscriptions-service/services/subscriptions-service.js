/* eslint-disable class-methods-use-this */
/* eslint-disable no-return-await */
import Axios from 'axios';
import dotenv from 'dotenv';
import models from '../models/index';
import ValidationError from '../../errors/validation-error';
import AmqpService from './amqp-service';

/** load env configs */
dotenv.config();

const { Subscription } = models;

class SubscriptionServices {
    #amqpService = null;

    constructor() {
        this.amqpService = new AmqpService(process.env.AMQP_CONNECTION_STRING, process.env.AMQP_CHANNEL_NAME, process.env.AMQP_QUEUE_NAME);
    }

    async findAll(userId) {
        return await Subscription.findAll({ where: { userId } });
    }

    async findOne(userId) {
        return await Subscription.findOne({ where: { userId } });
    }

    async create(subscription) {
       
        const response = await Axios.get(`http://localhost:3001/${subscription.planId}`);
        console.log(response.data);
        const plan = response.data;
        if (!plan) {
            throw new ValidationError(`Given plan id is invalid: ${subscription.planId}`);
        }
        await this.amqpService.init();
        subscription = await Subscription.create(subscription);
        return await this.amqpService.send(JSON.stringify({ plan, subscription }));
    }

    async deleteOne(id) {
        return await Subscription.destroy({ where: { id } });
    }
}

export default new SubscriptionServices();
