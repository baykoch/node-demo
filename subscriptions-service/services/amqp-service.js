import amqplib from 'amqplib';


class AmqpService {
    #channel = null;

    #connection = null;

    constructor(connectionUrl, channelName, queueName) {
        this.connectionUrl = connectionUrl;
        this.channelName = channelName;
        this.queueName = queueName;
    }

    async init() {
        try {
            if (this.connection && this.channel) {
                throw Error('Publisher rabbitmq connect error');
            }
            this.connection = await amqplib.connect(this.connectionUrl);
            this.channel = await this.connection.createChannel(this.channelName);
            return await this.channel.assertQueue(this.queueName);
        } catch (error) {
            throw error;
        }
    }

    async send(msg) {
        return this.channel.sendToQueue(this.queueName, Buffer.from(msg));
    }
}

export default AmqpService;
