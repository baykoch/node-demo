module.exports = {
    presets: ['@babel/preset-env'],
    plugins: [
        ['@babel/plugin-proposal-decorators', { decoratorsBeforeExport: false }],
        ['@babel/plugin-proposal-private-methods'],
        ['@babel/plugin-proposal-class-properties'],
        ['@babel/plugin-proposal-throw-expressions'],
        ['@babel/plugin-proposal-nullish-coalescing-operator'],
        ['@babel/plugin-proposal-optional-chaining', { loose: true }],
    ],
};
