/* eslint-disable import/first */
import express from 'express';
import dotenv from 'dotenv';
/** load env configs */
dotenv.config();

import SubscriptionsController from './controllers/subscriptions-controller';
import Middleware from '../middleware/middleware';
import ErrorHandlingMiddleware from '../middleware/error-handling';
import AuthenticationMiddleware from './middleware/auth';

// eslint-disable-next-line prefer-destructuring
const PORT = process.env.PORT;

const app = express();

Middleware(app);
AuthenticationMiddleware(app);

app.use('', SubscriptionsController);

// Error handler must be defined after all pther middleware/routes
ErrorHandlingMiddleware(app);

app.listen(PORT, () => {
    console.log(`Subscription service is running ${PORT}`);
});
