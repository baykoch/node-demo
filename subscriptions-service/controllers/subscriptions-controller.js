import express from 'express';
import asyncWrapper from '../../utilities/async-wrapper';
import subscriptionsService from '../services/subscriptions-service';
import validator from '../middleware/validator';
import protectedRotue from '../middleware/protected-route';


const router = express.Router();

router.use(protectedRotue());

// GET api/subscriptions
router.get('/', asyncWrapper(async(req, res) => {
    const userId = 2;
    const subscriptions = await subscriptionsService.findAll(userId);
    res.send(subscriptions);
}));

// GET api/subscriptions/:id
router.get('/:id', asyncWrapper(async(req, res) => {
    const {id} = req.params;
    const subscription = await subscriptionsService.findOne(id);
    res.send(subscription);
}));

// POST api/subscriptions
router.post('/', [validator('Subscription')], asyncWrapper(async(req, res) => {
    const subscription = await subscriptionsService.create(req.body);
    res.send(subscription);
}));

// DELETE api/subscriptions
router.delete('/:id', asyncWrapper(async(req, res) => {
    const {id} = req.params;
    await subscriptionsService.deleteOne(id);
    res.sendStatus(200);
}));

export default router;
