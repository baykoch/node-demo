import express from 'express';
import dotenv from 'dotenv';
import PlanController from './controllers/plans-controller';
import Middleware from '../middleware/middleware';
import ErrorHandlingMiddleware from '../middleware/error-handling';
import AuthenticationMiddleware from './middleware/auth';

// TODO:Dynamic import

/** load env configs */
dotenv.config();


// eslint-disable-next-line prefer-destructuring
const PORT = process.env.PORT;

const app = express();

Middleware(app);
AuthenticationMiddleware(app);

app.use('', PlanController);
// Error handler must be defined after all pther middleware/routes
ErrorHandlingMiddleware(app);

app.listen(PORT, () => {
    console.log(`Plan service is running ${PORT}`);
});
