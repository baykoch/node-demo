import express from 'express';
import asyncWrapper from '../../utilities/async-wrapper';
import plansService from '../services/plans-service';
import validator from '../middleware/validator';
import protectedRotue from '../middleware/protected-route';


const router = express.Router();

router.use(protectedRotue());

// GET api/plans
router.get('/', asyncWrapper(async(req, res) => {
    const userId = req.user;
    const plans = await plansService.findAll(userId);
    res.send(plans);
}));

// GET api/plans/:id
router.get('/:id', asyncWrapper(async(req, res) => {
    const { id } = req.params;
    const plan = await plansService.findOne(id);
    res.send(plan);
}));

// POST api/plans
router.post('/', [validator('Plan')], asyncWrapper(async(req, res) => {
    let plan = req.body;
    const userId = req.user;
    plan.userId = userId;
    plan = await plansService.create(plan);
    res.send(plan);
}));

// DELETE api/plans
router.delete('/:id', asyncWrapper(async(req, res) => {
    const { id } = req.params;
    await plansService.deleteOne(id);
    res.sendStatus(200);
}));

export default router;
