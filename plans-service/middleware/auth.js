import passport from 'passport';
import dotenv from 'dotenv';

import { Strategy as JwtStrategy, ExtractJwt as ExtractStrategy } from 'passport-jwt';


/** load env configs */
dotenv.config();

function AuthMiddleware(app) {
    const authStrategy = new JwtStrategy({
        secretOrKey: process.env.AUTH_SECRET,
        algorithms: ['HS256'],
        issuer: process.env.TOKEN_ISSUER,
        ignoreExpiration: false,
        jwtFromRequest: ExtractStrategy.fromAuthHeaderWithScheme('Bearer'), // Authorization: Bearer <TOKEN>
    }, async(payload, done) => {
        const id = parseInt(payload.sub, 10);
        if (id) {
            done(null, id);
        } else {
            done(null, false);
        }
    });
    
    passport.use(authStrategy);
    app.use(passport.initialize());
}

export default AuthMiddleware;
