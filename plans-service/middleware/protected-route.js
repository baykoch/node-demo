import possport from 'passport';

function ProtectedRoute() {
    return possport.authenticate('jwt', { session: false });
}

export default ProtectedRoute;
