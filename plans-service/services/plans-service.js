/* eslint-disable class-methods-use-this */
/* eslint-disable no-return-await */
import models from '../models/index';
import CachingService from './caching-service';

const { Plan } = models;

class PlansService {
    constructor() {
        this.cachingService = new CachingService({
            host: 'localhost', // process.env.REDIS_HOST,
            port: '6380', // process.env.REDIS_POST,
            password: '', // process.env.REDIS_PASSWROD,
        });
    }

    async findAll (userId) {
        let plans = await this.cachingService.getPlans(userId);
        if (plans) {
            return plans;
        }
        plans = await Plan.findAll({ where: { userId } });
        await this.cachingService.storePlans(userId, plans);
        return plans;
    }

    async findOne (userId) {
        let plan = await this.cachingService.getPlan(userId);
        console.log(plan)
        if (plan) {
            return plan;
        }
        plan = await Plan.findOne({ where: { userId } });
        await this.cachingService.storePlan(userId, plan);
        return plan;
    }

    async create (plan) {
        return await Plan.create(plan);
    }

    async deleteOne (id) {
        const plan = await this.findOne(id);
        if (plan) {
            await this.cachingService.purgeCache(plan.userId);
            return await Plan.destroy({ where: { id } });
        }
        return Promise.resolve();
    }
}

export default new PlansService();
