import redis from 'redis';
// eslint-disable-next-line object-curly-spacing
import {promisify} from 'util';

class CachingService {
    constructor(redisConfig) {
        this.plansKey = 'plans';
        this.redisHost = redisConfig.host;
        this.redisPort = redisConfig.port;
        this.redisPassword = redisConfig.password;
        this.redisClient = redis.createClient({
            host: this.redisHost,
            port: this.redisPort,
            auth_pass: this.redisPassword,
        });
    }

    async storePlans(userId, plans) {
        plans = plans || [];
        await this.purgeCache(userId);
        await promisify(this.redisClient.hset)
            .bind(this.redisClient)(CachingService.getUserKey(userId), this.plansKey, JSON.stringify(plans));
    }

    async getPlans(userId) {
        const plans = await promisify(this.redisClient.hget)
            .bind(this.redisClient)(CachingService.getUserKey(userId), this.plansKey);
        return plans ? JSON.parse(plans) : null;
    }

    async storePlan(userId, plan) {
        plan = plan || {};
        await this.purgeCache(userId);
        await promisify(this.redisClient.hset)
            .bind(this.redisClient)(CachingService.getUserKey(userId), this.plansKey, JSON.stringify(plan));
    }

    async getPlan(userId) {
        const plans = await promisify(this.redisClient.hget)
            .bind(this.redisClient)(CachingService.getUserKey(userId), this.plansKey);
        return plans ? JSON.parse(plans) : null;
    }

    async purgeCache(userId) {
        if (!userId) {
            return Promise.resolve();
        }
        return promisify(this.redisClient.expire)
            .bind(this.redisClient)(CachingService.getUserKey(userId), 0);
    }

    static getUserKey(userId) {
        return `user#${userId}`;
    }
}

export default CachingService;
