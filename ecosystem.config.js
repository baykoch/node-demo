module.exports = {
    apps: [
        {
            name: 'plans-service',
            exec_interpreter: 'nodemon --exec babel-node --ignore=node_modules index.js',
            script: './plans-service/index.js',
            watch: true,
            env: {
                NODE_ENV: 'development',
                MYSQL_USER: 'root',
                MYSQL_PASS: '123456_my',
                MYSQL_HOST: 'localhost',
                MYSQL_PORT: 3308,
                MYSQL_DB: 'PlansDB',
                PORT: 3001,
            },
            env_production: {
                NODE_ENV: 'production',
            },
        },
        {
            name: 'subscriptions-service',
            exec_interpreter: 'nodemon --exec babel-node --ignore=node_modules index.js',
            script: './subscriptions-service/index.js',
            watch: true,
            env: {
                NODE_ENV: 'development',
                MYSQL_USER: 'root',
                MYSQL_PASS: '123456_my',
                MYSQL_HOST: 'localhost',
                MYSQL_PORT: 3307,
                MYSQL_DB: 'SubscriptionsDB',
                PORT: 3002,
            },
            env_production: {
                NODE_ENV: 'production',
            },
        },
    ],
};
