import Joi from '@hapi/joi';

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
    }, {});
    User.associate = function(models) {
    // associations can be defined here
    };
    return User;
};

module.exports.UserValidationSchema = Joi.object().keys({

    name: Joi.string().required(),
    surname: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required().min(5).max(32),

});

module.exports.LoginValidationSchema = Joi.object().keys({

    email: Joi.string().email().required(),
    password: Joi.string().required(),

});
