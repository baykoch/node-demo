import express from 'express';
import dotenv from 'dotenv';
import UserController from './controllers/user-controller';
import Middleware from '../middleware/middleware';
import ErrorHandlingMiddleware from '../middleware/error-handling';

// TODO:Dynamic import

/** load env configs */
dotenv.config();


// eslint-disable-next-line prefer-destructuring
const PORT = process.env.PORT;

const app = express();

Middleware(app);
app.use('', UserController);
// Error handler must be defined after all other middleware/routes
ErrorHandlingMiddleware(app);

app.listen(PORT, () => {
    console.log(`Auth service is running ${PORT}`);
});
