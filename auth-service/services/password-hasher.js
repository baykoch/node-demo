import bcrypt from 'bcrypt';

class PasswordHasher {
    static #rounds = 10;

    async hash (password) {
        return await bcrypt.hash(password, PasswordHasher.#rounds);
    }

    async check (passwrod, hash) {
        return await bcrypt.compare(passwrod, hash);
    }
}

export default PasswordHasher;
