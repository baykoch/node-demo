/* eslint-disable class-methods-use-this */

import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';
import models from '../models/index';
import PasswordHasher from './password-hasher';

/** load env configs */
dotenv.config();


const { User } = models;

class UsersService {
  
    #passwordHasher = null;

    constructor() {
        this.#passwordHasher = new PasswordHasher();
    }
    async create (user) {
        user.password = await this.#passwordHasher.hash(user.password);
        user = await User.create(user);
        return this.#generateAccessToken(user);
    }
    
    async findByEmail (email) {
        return await User.findOne({ where: { email } });
    }

    async signIn (email, password) {
        const user = await this.findByEmail(email);
     
        if (!user) {
            return null;  // TODO : Throw 
        }
        const checkPassword = await this.#passwordHasher.check(password, user.password);
        if (checkPassword) {
            return this.#generateAccessToken(user);
        } else {
            return null; // TODO : Throw 
        }
    }

    #generateAccessToken (user) {
        if (!user) {
            throw new Error('Invalid user');  // TODO : Throw 
        }
        const userInfo = user.toJSON();
        delete userInfo.password;
        const payload = {
            user: userInfo,
        };
        const token = jwt.sign(payload, process.env.AUTH_SECRET, {
            algorithm: 'HS256',
            issuer: process.env.TOKEN_ISSUER,
            subject: `${user.id}`,
        });
        return token;
    }
}

export default new UsersService();