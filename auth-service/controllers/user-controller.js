import express from 'express';
import asyncWrapper from '../../utilities/async-wrapper';
import validator from '../middleware/validator';
import AuthenticationError from '../../errors/authentication-error';
import UsersService from '../services/users-service';

const router = express.Router();

// POST api/
router.post('/sign-up', [validator('User')], asyncWrapper(async(req, res) => {
    const token = await UsersService.create(req.body);
    res.send(token);
}));

// POST api/
router.post('/sign-in', [validator('User', 'login')], asyncWrapper(async(req, res) => {
    const { email, password } = req.body;
    const token = await UsersService.signIn(email, password);
    if (!token) {
        throw new AuthenticationError('Invalid credentials');
    } else {
        res.send(token);
    }
}));

export default router;
