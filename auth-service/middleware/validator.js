import Joi from '@hapi/joi';
import User from '../models/user';
import ValidationError from '../../errors/validation-error';

/**
 * ! Farkli validation modelleri tanımlama bir schema için örnek update
 * * default create veya varsayılan plan gibi yazılabilir.
 */
const validators = {
    User: {
        scopes: {
            default: User.UserValidationSchema,
            login: User.LoginValidationSchema,
        },
    },
};

function scopeExists(validator, scope) {
    return Object.keys(validator.scopes).find(key => key === scope) !== undefined;
}

function getSchema(model, scope) {
    const validator = validators[model];
    if (!validator) {
        throw new Error('Validator does not exist');
    }
    // First check if the given validator has multiple scopes
    if (validator.scopes) {
        // If the caller has passed a value for 'scope'
        if (scope) {
            if (!scopeExists(validator, scope)) {
                throw new Error(`Scope ${scope} does not exist in ${model} validator`);
            } else {
                return validator.scopes[scope];
            }
        } else {
            return validator.scopes.default;
        }
    } else {
        return validator;
    }
}

function validate(model, object, scope) {
    return Joi.validate(object, getSchema(model, scope), {
        allowUnknown: true,
    });
}

// Actual middleware factory
export default function ValidationMiddleware(model, scope) {
    return (req, res, next) => {
        const validationResult = validate(model, req.body, scope);
        if (validationResult.error) {
            throw new ValidationError(validationResult.error.message, model);
        } else {
            next();
        }
    };
}
