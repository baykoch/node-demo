import amqplib from 'amqplib';

class PaymentsService {
    #channel = null;

    #connection = null;

    constructor(connectionUrl, channelName, queueName) {
        this.connectionUrl = connectionUrl;
        this.channelName = channelName;
        this.queueName = queueName;
    }

    async init() {
        console.log(`Initializing RabbitMQ connect to ${this.connectionUrl}`);
        this.connection = await amqplib.connect(this.connectionUrl);
        this.channel = await this.connection.createChannel(this.channelName);
        await this.channel.assertQueue(this.queueName);
        await this.channel.consume(this.queueName, this.onMessageReceived.bind(this));
    }

    onMessageReceived(message) {
        const parsed = JSON.parse(message.content.toString());
        if (parsed ?.subscription && parsed ?.plan) {
            const { plan, subscription } = parsed;
            console.log(`Charging card ${subscription.cardNumber} with ${plan.price}`);
            this.channel.ack(message);
        } else {
            console.log('Invalid payload.');
            this.channel.ack(message);
        }
    }
}

export default PaymentsService;

